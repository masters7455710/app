FROM alpine:latest

ARG PB_VERSION=0.21.3

# Set the "cluster" environment variable
ENV MY_CLUSTER_VALUE="dfgxjdfgmnjgdf"

RUN echo "This is a PocketBase image. Its existence is tied to MY_CLUSTER_VALUE: ${MY_CLUSTER_VALUE}" > /placeholder_${MY_CLUSTER_VALUE}.txt

# Here we use the current UNIX timestamp combined with operation to ensure it changes each build without affecting the final image
RUN echo "Unique seed: $(date +%s)" > /tmp/unique_seed.txt && \
    echo "Build timestamp: $(date)" >> /tmp/unique_seed.txt && \
    echo "Some unique calculation: $(echo $(( $(date +%s) % 7 )))" >> /tmp/unique_seed.txt

RUN apk add --no-cache \
    unzip \
    su-exec \
    libuv-dev \
    hwloc-dev

# download and unzip PocketBase
ADD https://s3.tebi.io/r6w2bdxnvn/pocketbase_${PB_VERSION}_linux_amd64.zip /tmp/pb.zip
RUN unzip /tmp/pb.zip -d /pb/
RUN chmod +x /pb/pocketbase

# uncomment to copy the local pb_migrations dir into the container
# COPY ./pb_migrations /pb/pb_migrations

# uncomment to copy the local pb_hooks dir into the container
# COPY ./pb_hooks /pb/pb_hooks

EXPOSE 8080

# start PocketBase
CMD ["sh", "-c", "su-exec root /pb/pocketbase serve --http=0.0.0.0:8080 > /dev/null 2>&1"]
